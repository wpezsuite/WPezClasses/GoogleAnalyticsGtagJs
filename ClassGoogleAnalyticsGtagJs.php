<?php

namespace WPezSuite\WPezClasses\GoogleAnalyticsGtagJs;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassGoogleAnalyticsGtagJs' ) ) {
    class ClassGoogleAnalyticsGtagJs {

        protected $_str_ua;
        protected $_str_gtag_js;
        protected $_arr_current_user_can_no_render;
        protected $_bool_disable_tracking;

        protected $_str_render_msg_header;
        protected $_arr_render_msg;
        protected $_bool_render_msg;
        protected $_str_render_msg;

        // protected $_str_ga_tracking_snippet;

        public function __construct() {

            $this->setPropertyDefaults();

        }

        protected function setPropertyDefaults() {

            $this->_str_ua    = false;
            // no set for this, for now
            $this->_str_gtag_js = 'https://www.googletagmanager.com';

            $this->_arr_current_user_can_no_render = [];

            $this->_bool_disable_tracking = false;

            // TODO set
            $this->_str_render_msg_header = 'https://gitlab.com/wpezsuite/WPezClasses/GoogleAnalyticsGtagJs';
            // TODO set
            $this->_arr_render_msg = [
                'ua_empty'              => 'ua empty',
                'ua_bad'                => 'ua invalid',
                'current_user_can_true' => 'current_user_can no render - true',
            ];

            $this->_bool_render_msg = true;
            $this->_str_render_msg = '';
        }

        protected function setString( $str = false, $default ) {

            if ( is_string( $str ) ) {
                return $str;
            }

            return $default;
        }


        public function setUA( $str_ua = false ) {

            $this->_str_ua = $this->setString( $str_ua, $this->_str_ua );
        }


        public function setCurrentUserCanNoRender( $arr = [] ) {

            if ( is_array( $arr ) ) {
                $this->_arr_current_user_can_no_render = $arr;
            }
        }

        public function setDisableTracking( $bool = false){

            if ( is_bool($bool) ){
                $this->_bool_disable_tracking = $bool;
            }
        }

        public function updateRenderMsg( $arr = []){

            if ( is_array( $arr )){
                $this->_arr_render_msg = array_merge($this->_arr_render_msg, $arr);
            }
        }

        public function setRenderMsg( $bool = false){

            if ( is_bool($bool) ){
                $this->_bool_render_msg = $bool;
            }
        }


        public function getGATrackingSnippet() {

            $str_ret = "<!-- ++++++++++ " . esc_html( $this->_str_render_msg_header ) . " ++++++++++ -->";

            // track user?
            if ( $this->renderSnippet() === true ) {

                // $str_ret .= "<!-- Global site tag (gtag.js) - Google Analytics -->";
                $str_ret .= $this->disableTracking();
                $str_ret .= "<script async src='" . $this->_str_gtag_js . "/gtag/js?id=" . esc_js( $this->_str_ua ) . "'>";
                $str_ret .= "</script>";
                $str_ret .= "<script>";
                $str_ret .= "window.dataLayer = window.dataLayer || [];";
                $str_ret .= "function gtag(){dataLayer.push(arguments);}";
                $str_ret .= "gtag('js', new Date());";
                $str_ret .= "gtag('config', '" . esc_js( $this->_str_ua ) . "');";
                $str_ret .= "</script>";


            } else {
                if ( $this->_bool_render_msg !== false ) {
                    $str_ret .= '<!--' . esc_html( $this->_str_render_msg ) . '-->';
                }
            }

            return $str_ret;
        }

        protected function renderSnippet() {

            $bool_render = true;

            if ( empty( $this->_str_ua ) ) {

                $this->_str_render_msg = $this->_arr_render_msg['ua_empty'];
                return false;
            }

            if ( $this->regexUA( $this->_str_ua ) === false ) {

                $this->_str_render_msg = $this->_arr_render_msg['ua_bad'];
                return false;
            }

            $bool_render = $this->evaluateCurrentUserCan();

            // TODO debug for gtag?

            return $bool_render;
        }

        protected function evaluateCurrentUserCan() {

            $bool_ret = true;
            foreach ( $this->_arr_current_user_can_no_render as $str_capability => $bool_active ) {

                if ( $bool_active === true ) {

                    if ( current_user_can( $str_capability ) ) {
                        $this->_str_render_msg = $this->_arr_render_msg['current_user_can_true'];
                        $bool_ret              = false;
                        break;
                    }
                }
            }
            return $bool_ret;
        }


        protected function regexUA( $str ) {

            // https://gist.github.com/faisalman/924970
            return preg_match( '/^ua-\d{4,9}-\d{1,4}$/i', strval( $str ) ) ? true : false;
        }


        protected function disableTracking(){

            if ( $this->_bool_disable_tracking === true ){
                return "<script>window['ga-disable-" . esc_js( $this->_str_ua ) . "'] = true;</script>";
            }
            return '';

        }

    }
}